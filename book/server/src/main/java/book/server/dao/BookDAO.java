package book.server.dao;

import static book.server.dao.QBook.book;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQueryFactory;

import techbase.server.dao.GenericDAO;

@Named
@Singleton
public class BookDAO extends GenericDAO<Book> {
	
	public List<Book> findAll() {
		final JPQLQuery<Book> query = new JPAQueryFactory(getEntityManager()).
				selectFrom(book).
				where(book.isValid()).
				orderBy(book.id.asc());
		
		return query.fetch();
	}
	
	public void delete(Long isbn) {
		final JPADeleteClause query = new JPAQueryFactory(getEntityManager()).
				delete(book).
				where(book.isbn.eq(isbn));
		
		query.execute();
	}
	
	public boolean isExisting(Long bookId) {
		final JPQLQuery<Book> query = new JPAQueryFactory(getEntityManager()).
				selectFrom(book).
				where(book.id.eq(bookId));
		
		return query.fetchCount() > 0;
	}
	
	public Book findBy(Long isbn) {
		final JPQLQuery<Book> query = new JPAQueryFactory(getEntityManager()).
				selectFrom(book).
				where(book.isbn.eq(isbn));
		
		return query.fetchFirst();
	}
}
