package book.server.remote.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.transaction.Transactional;

import book.common.to.BookDto;
import book.server.logic.MaintenanceBookService;
import book.server.logic.SearchBookService;
import book.server.remote.BookingApi;
import techbase.server.logic.logging.Logged;
import techbase.server.logic.validation.ValidatorService;

@Logged
@Transactional
@Named
@Singleton
public class BookingApiImpl implements BookingApi {

    @Inject
    private SearchBookService searchBookService;

    @Inject
    private MaintenanceBookService maintenanceBookService;
    
    @Inject
    private ValidatorService validatorService;

    @Override
    public List<BookDto> retrieveAll() {
        return this.searchBookService.retrieveAll();
    }

    @Override
    public BookDto save(BookDto book) {
    	this.validatorService.validate(book);
    	
        return this.maintenanceBookService.save(book);
    }

    @Override
    public void delete(Long isbn) {
        this.maintenanceBookService.delete(isbn);
    }

}
