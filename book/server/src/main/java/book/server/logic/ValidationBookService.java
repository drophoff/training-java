package book.server.logic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import book.common.to.BookDto;
import book.server.domain.BookConstant;
import book.server.domain.BookManager;
import techbase.server.logic.exception.http.ConstraintType;
import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.logging.Logged;
import techbase.server.logic.validation.GlobalValidator;

@Logged
@Named
@Singleton
public class ValidationBookService implements GlobalValidator<BookDto> {
	private final static String ATTRIBUTE_ISBN = "#ISBN";
	private final static String ATTRIBUTE_NAME = "#Name";
	private final static String ATTRIBUTE_PRICE = "#Price";
	private final static String ENTITY_NAME = "Book";
	
    @Inject
    private BookManager bookManager;

    @Override
    public List<ConstraintViolation> validate(BookDto book) {
        List<ConstraintViolation> messages = new ArrayList<>();

        checkMandatoryAttributes(book, messages);
        checkConstrains(book, messages);
        checkKeyExistence(book, messages);

        return messages;
    }


	private void checkMandatoryAttributes(BookDto book, List<ConstraintViolation> messages) {
        if (book.getIsbn() == null) {
            messages.add(new ConstraintViolation(ConstraintType.REQUIRED, ENTITY_NAME + ATTRIBUTE_ISBN));
        }

        if (book.getName() == null || book.getName().isEmpty()) {
            messages.add(new ConstraintViolation(ConstraintType.REQUIRED, ENTITY_NAME + ATTRIBUTE_NAME));
        }

        if (book.getPrice() == null) {
            messages.add(new ConstraintViolation(ConstraintType.REQUIRED, ENTITY_NAME + ATTRIBUTE_PRICE));
        }
        
        if (book.getVersion() == null && book.getId() != null) {
        	messages.add(new ConstraintViolation(ConstraintType.REQUIRED, ENTITY_NAME + "#Version"));
        }
    }

    private void checkConstrains(BookDto book, List<ConstraintViolation> messages) {
        if (messages.isEmpty()) {
            if (book.getName() != null && book.getName().length() > 50) {
                messages.add(new ConstraintViolation(ConstraintType.MAX, ENTITY_NAME + ATTRIBUTE_NAME));
            }

            if (book.getIsbn() != null && (book.getIsbn() < BookConstant.ISBN_MIN || book.getIsbn() > BookConstant.ISBN_MAX)) {
                messages.add(new ConstraintViolation(ConstraintType.PATTERN, ENTITY_NAME + ATTRIBUTE_ISBN));
            }

            if (book.getPrice() != null && book.getPrice().compareTo(BigDecimal.ZERO) <= 0) {
                messages.add(new ConstraintViolation(ConstraintType.MIN, ENTITY_NAME + ATTRIBUTE_PRICE));
            }
        }
    }

    private void checkKeyExistence(BookDto book, List<ConstraintViolation> messages) {
		if (messages.isEmpty()) {
			final BookDto bookWithIsbn = bookManager.findBy(book.getIsbn());
			
			if (book.getId() != null) {
				if (!bookManager.isExisting(book.getId())) {				
					messages.add(new ConstraintViolation(ConstraintType.PATTERN, ENTITY_NAME + "#Id"));
				}
				
				if (bookWithIsbn == null || !bookWithIsbn.getId().equals(book.getId())) {
					messages.add(new ConstraintViolation(ConstraintType.UNIQUE, ENTITY_NAME + ATTRIBUTE_ISBN));
				}
			}
			
			if (book.getId() == null && bookWithIsbn != null) {
				messages.add(new ConstraintViolation(ConstraintType.UNIQUE, ENTITY_NAME + ATTRIBUTE_ISBN));
			}
		}
	}
    
    @Override
    public Class<BookDto> isResponsibleFor() {
        return BookDto.class;
    }
}
