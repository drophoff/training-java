package book.server.logic;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import book.common.to.BookDto;
import book.server.domain.BookManager;
import techbase.server.logic.logging.Logged;

@Logged
@Named
@Singleton
public class MaintenanceBookService {

    @Inject
    private BookManager bookManager;

    public BookDto save(BookDto book) {
        return this.bookManager.save(book);
    }

    public void delete(Long isbn) {
        this.bookManager.delete(isbn);
    }

}
