package book.server.logic;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import book.common.to.BookDto;
import book.server.domain.BookManager;
import techbase.server.logic.logging.Logged;

@Logged
@Named
@Singleton
public class SearchBookService {

    @Inject
    private BookManager bookManager;

    public List<BookDto> retrieveAll() {
        return this.bookManager.retrieveAll();
    }
}
