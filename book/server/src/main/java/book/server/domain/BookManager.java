package book.server.domain;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import book.common.to.BookDto;
import book.server.dao.Book;
import book.server.dao.BookDAO;
import techbase.server.logic.logging.Logged;

@Logged
@Named
@Singleton
public class BookManager {

    @Inject
    private BookDAO bookDAO;

    @Inject
    private BookMapper mapper;

    public List<BookDto> retrieveAll() {
        return this.mapper.mapPersistent(this.bookDAO.findAll());
    }

    public BookDto save(final BookDto book) {
        final Book unsaved = this.mapper.map(book);
        final Book saved = this.bookDAO.save(unsaved);
        return this.mapper.map(saved);
    }

    public void delete(Long isbn) {
        if (isbn > 0) {
            this.bookDAO.delete(isbn);
        }
    }
    
    public boolean isExisting(Long bookId) {
    	boolean isExisting = false;
    	
    	if (bookId != null) {
    		isExisting = this.bookDAO.isExisting(bookId);
    	}
    	
    	return isExisting;
    }
    
    public BookDto findBy(Long isbn) {
    	BookDto found = null;
    	
    	if (isbn != null) {
    		found = this.mapper.map(this.bookDAO.findBy(isbn));
    	}
    	
    	return found;
    }
}
