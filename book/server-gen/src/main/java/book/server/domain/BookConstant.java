package book.server.domain;

public final class BookConstant {
	// Minimal allowed value for ISBN
	public static final Long ISBN_MIN = Long.valueOf(1000000000000l);
	// Maximal allowed value for ISBN
	public static final Long ISBN_MAX = Long.valueOf(99999999999999l);

	private BookConstant() {
		// Avoid public instance creation
	}
}
