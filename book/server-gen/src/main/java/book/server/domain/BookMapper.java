package book.server.domain;

import java.util.List;

import org.mapstruct.Mapper;

import book.common.to.BookDto;
import book.server.dao.Book;

@Mapper(componentModel = "jsr330")
public interface BookMapper {
    BookDto map(Book source);

    Book map(BookDto source);

    List<BookDto> mapPersistent(List<Book> source);

    List<Book> mapTransient(List<BookDto> source);
}
