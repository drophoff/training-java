package book.server.dao;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "BOOK")
public class Book {

	@ToString.Include
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BOOK-SEQUENCE-GEN")
	@SequenceGenerator(name = "BOOK-SEQUENCE-GEN", sequenceName = "SEQ_BOOK_ID", allocationSize = 50)
	private Long id;

    @Column(nullable= false, length = 50)
	private String name;

    @Column(nullable= false, precision=10, scale=2)
	private BigDecimal price;

	@Column(nullable= false, length = 13)
	private Long isbn;

	@Column(nullable= false)
	@Version
	private Long version;
}
