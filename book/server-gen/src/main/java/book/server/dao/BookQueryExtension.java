package book.server.dao;

import java.math.BigDecimal;

import com.querydsl.core.annotations.QueryDelegate;
import com.querydsl.core.types.dsl.BooleanExpression;

import book.server.domain.BookConstant;

public class BookQueryExtension {

	/**
	 * Checks if the given book is valid.
	 * 
	 * @param book contains the metadata of the checked object
	 * @return true if the book is valid, otherwise false
	 */
	@QueryDelegate(Book.class)
	public static BooleanExpression isValid(QBook book) {
		return book.price.goe(BigDecimal.ZERO).
				and(book.isbn.between(BookConstant.ISBN_MIN, BookConstant.ISBN_MAX));
	}
}
