package book.server;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import book.server.dao.Book;

@Configuration
@ComponentScan(basePackageClasses = BookConfiguration.class )
@EntityScan(basePackageClasses = Book.class)
public class BookConfiguration {
}
