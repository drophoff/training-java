# Client

## Project setup
```
npm i -g @vue/cli @vue/cli-service-global
vue create .
vue add vuetify
```

## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```

## Lints and fixes files
```
npm run lint
```

## Deployment

Previewing Locally
```
npm install -g serve
serve -s dist
```
