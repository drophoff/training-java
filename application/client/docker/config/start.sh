#!/bin/bash
. /etc/environment


function onDockerFinish {
   /etc/init.d/nginx stop

   exit 0
}


function onDockerStart {
   /etc/init.d/nginx start &

   /usr/bin/tail -s 10 -f /dev/null & wait
}


trap onDockerFinish SIGTERM
onDockerStart