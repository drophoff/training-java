module.exports = {
  "devServer": {
    "proxy": {
      "/rest/v1/book": {
        "target": {
          "host": "localhost",
          "protocol": "http",
          "port": "8081"
        },
        "logLevel": "debug",
        "changeOrigin": true,
        "secure": false
      }
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}
