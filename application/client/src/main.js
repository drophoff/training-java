import Vue from 'vue'
import App from './App.vue'
import vuex from './plugins/vuex';
import vuetify from './plugins/vuetify';
import vuelidate from './plugins/vuelidate';
import store from "./store";
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.config.productionTip = false

new Vue({
  vuex,
  store,
  vuelidate,
  vuetify,
  render: h => h(App)
}).$mount('#app')
