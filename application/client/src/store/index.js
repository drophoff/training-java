import Vuex from "../plugins/vuex"
import Book from "./book.module";

export default new Vuex.Store({
    modules: {
        Book
    }
  });