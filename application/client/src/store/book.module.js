import { retrieveAllBooks, saveBook, deleteBook } from "@/api/book.api"

const state = {
    books: []
}

const getters = {
    getBooks(state) {
        return state.books;
    }
}

const actions = {
    async fetchBooks({ commit }) {
        try {
            const response = await retrieveAllBooks();
            commit('SET_BOOKS', response.data);
        } catch (error) {
            // Nothing to do, due to generic exception handler
        }
    },

    async addBook({ commit }, book ) {
        try {
            const response = await saveBook(book);
            commit('ADD_BOOK', response.data);
        } catch (error) {
            // Nothing to do, due to generic exception handler
        }
    },

    async editBook({ commit }, book ) {
        try {
            const response = await saveBook(book);
            commit('EDIT_BOOK', response.data);
        } catch (error) {
            // Nothing to do, due to generic exception handler
        }
    },

    async deleteBook({ commit }, isbn ) {
        try {
            await deleteBook(isbn);
            commit('DELETE_BOOK', isbn);
          } catch (error) {
            // Nothing to do, due to generic exception handler
          }
    }
}

const mutations = {
    SET_BOOKS(state, books) {
        state.books = books;
    },
    ADD_BOOK(state, book) {
        state.books = [...state.books, book];
    },
    EDIT_BOOK(state, changedBook) {
        state.books = state.books.map(book => 
            book.id === changedBook.id ? changedBook : book
        );
    },
    DELETE_BOOK(state, isbn) {
        state.books = state.books.filter(book =>
            book.isbn !== isbn
        );
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
