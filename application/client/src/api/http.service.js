
import axios from "axios";

const HttpService = axios.create({
    timeout: 4500,
    headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Application-Id": "Book",
        "Application-Env": process.env.VUE_APP_ENV_NAME
    }
});

const errorInterceptor = (error) => {
    if (error.response !== undefined) {
        if (error.response.data !== undefined) {
          console.error(JSON.stringify(error.response.data));
        } else {
          console.error(error.response.status, error.message);
        }
    } else {
        console.error(error.message);
    }

    return Promise.reject(error);
}

const responseHandler = (response) => {
    let message = "Http status: " + response.status;
  
    if (response.status >= 100 && response.status <= 199) {
        console.info( message );
    } else if (response.status >= 300 && response.status <= 399) {
        console.warn( message );
    } else if (response.status >= 400) {
        console.error( message );
    }

    return response;
}

HttpService.interceptors.response.use(
  response => responseHandler(response),
  error => errorInterceptor(error)
);

export default HttpService;
