import HttpService from './http.service';

const BOOK_END_POINT = '/rest/v1/book';

/**
 * Returns all available books.
 */
const retrieveAllBooks = () => HttpService.get(BOOK_END_POINT + '/listAll');

/**
 * Save the provided book.
 * 
 * @param {book} book that shall be saved.
 */
const saveBook = (book) => HttpService.post(BOOK_END_POINT, JSON.stringify(book));

/**
 * Deletes the book.
 * 
 * @param {Number} isbn that identifies the book.
 */
const deleteBook = (isnb) => HttpService.delete(BOOK_END_POINT + `/${isnb}`);

export {
    retrieveAllBooks,
    saveBook,
    deleteBook
}
