package bookstore;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import book.common.to.BookDto;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import techbase.server.logic.config.values.GlobalConfiguration;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles({"logging"})
public class ApplicationTests {
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationTests.class);
	
	@Inject
    private GlobalConfiguration config;

	@PostConstruct
	private void initialize() {
		LOG.debug("Initialize Test Framework!");
		
		RestAssured.port = config.getServer().getPort();
		RestAssured.basePath = "/rest/v1/book";
	}
	
    @Test
    public void testListAll() {
    	final BookDto[] books = RestAssured.
    		given().
    		when().
    			contentType(ContentType.JSON).
    			get("/listAll").
    		then().
    			statusCode(HttpStatus.SC_OK).
    			contentType(ContentType.JSON).
    		assertThat().
    			body("size()", Matchers.is(5)).
    			body("id",     Matchers.hasItems(Matchers.is(2), Matchers.is(3))).
    			body("isbn",   Matchers.hasItems(Matchers.is(9783864905254l), Matchers.is(9780321213358l), Matchers.is(9780134052502l))).
    		extract().
    			as(BookDto[].class);
    	
    	Assertions.assertNotNull(books);
    }
    
    @Test
    public void testUpdate() {
    	final BookDto adjust = new BookDto();
    	adjust.setId(Long.valueOf(2));
    	adjust.setName("Hibernate in Action 2");  // Change 01
    	adjust.setIsbn(9780134757599l);
    	adjust.setPrice(new BigDecimal("49.99")); // Change 02
    	adjust.setVersion(Long.valueOf(1));
    	
    	RestAssured.
			given().
				contentType(ContentType.JSON).
			    body(adjust).
			when().
			    post().
			then().
				statusCode(HttpStatus.SC_OK).
				contentType(ContentType.JSON).
			assertThat().
				body("id",      Matchers.is(adjust.getId().intValue())).
				body("name",    Matchers.is(adjust.getName())).
				body("isbn",    Matchers.is(adjust.getIsbn().longValue())).
				body("price",   Matchers.is(adjust.getPrice().floatValue())).
				body("version", Matchers.is(2));
    }
    
    @Test
    public void testDelete() {
        RestAssured.
            given().
                contentType(ContentType.JSON).
                pathParam("bookId", Long.valueOf(4)).
            when().
                delete("/{bookId}").
            then().
                statusCode(HttpStatus.SC_NO_CONTENT);
    }
}
