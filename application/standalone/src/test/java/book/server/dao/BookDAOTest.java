package book.server.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class BookDAOTest {
    
    private EntityManagerFactory factory = Persistence.createEntityManagerFactory("h2");
    private EntityManager em = factory.createEntityManager();
    
    @Test
    public void testFindAll_noDataExisting_emptyResponse() {
        BookDAO bookDAO = Mockito.mock(BookDAO.class);
        Mockito.when(bookDAO.getEntityManager()).thenReturn(em);
        Mockito.when(bookDAO.findAll()).thenCallRealMethod();
        
        List<Book> books = bookDAO.findAll();
        
        Assertions.assertNotNull(books, "Result should be empty but never null!");
        MatcherAssert.assertThat(books, Matchers.empty());
    }
}
