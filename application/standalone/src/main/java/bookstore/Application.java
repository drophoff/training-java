package bookstore;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import book.server.BookConfiguration;
import techbase.server.ApplicationConfiguration;
import techbase.server.logic.config.values.GlobalConfiguration;

@Import({ ApplicationConfiguration.class, BookConfiguration.class })
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Inject
    private GlobalConfiguration config;

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(Application.class);
        application.run(args);
    }

    public void run(String... args) throws Exception {
        final Logger logger = LoggerFactory.getLogger(Application.class);

        logger.info("Environment: {}", config.getEnvironment());
        logger.info("Port: {}", config.getServer().getPort());

    }
}
