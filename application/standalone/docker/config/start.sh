#!/bin/bash
. /etc/environment


function onDockerFinish {
   pkill java

   exit 0
}


function onDockerStart {
   DEBUG_OPTS="";
   if [ "${DEBUG_ENABLED}" == "true" ]; then
     DEBUG_OPTS="-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n";
   fi

   /etc/alternatives/java -XX:+UseContainerSupport -XX:MaxRAMPercentage=95.0 $DEBUG_OPTS -jar "${APP_FOLDER_BIN}/${APP_NAME_BIN}" &

   /usr/bin/tail -s 10 -f /dev/null & wait
}


trap onDockerFinish SIGTERM
onDockerStart
