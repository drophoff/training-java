package bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import book.server.BookConfiguration;
import techbase.server.ApplicationConfiguration;

@Import({ ApplicationConfiguration.class, BookConfiguration.class })
@SpringBootApplication
public class WebApplication extends SpringBootServletInitializer {
	
	public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebApplication.class).profiles("webapp");
    }
}
