# Introduction

This project is a minimal project designed for training only.
The application consists of a back-end and a front-end.
The back-end provides functions for searching and editing books via Rest-Endpoints.
The front-end uses these Rest-Endpoints to display and maintain the data.

The application uses the following technical libraries:
* Vue
* Java 11
* Spring Boot
* Spring Data (JPA 2.1)
* QueryDsl
* Hibernate
* Apache CXF
* Rest Assured
* Liquibase
* Lambok
* Mapstruct
* Logback plus slf4j or Java Logging API
* OpenAPI Generator

Furthermore it provides the features:
* Maven (Multi Module)
* Four-Layer-Architecture
* Transportobjects & Transformation handling
* Validationhandling
* Exceptionhandling
* Support for different deployments types (see application folder)
    * Front-End in a Plain-HTTP-Server (client)
    * Front-End in a Servlet-Container (web)
    * Back-End as a Spring Boot Fat-JAR (standalone)
    * Back-End in a Java SE Tomcat (war)
    * Back-End in a Web Application Server (EE) or Eclipse MicroProfile Server (jee)

## Build

The project can be built from the project directory using the following commands on the console (cmd):

1.  `mvn clean install -f ./bom`
2.  `mvn clean install -f ./parent`
3.  `mvn clean install -f ./aggregator`

The defined execution sequence must be observed here.
If the source code files are changed, only the aggregator module needs to be built.


## Application Start

After the application has been started, the ports (see application.yml) are used for the entire operating system.
The application can only be restarted if no other application instance is active and the ports are free.

The starting process is described in the following sections.


### Back-End

* Change into the directory: /application/standalone
* `java -Dspring.profiles.active=default,standalone -jar target/standalone.jar`

Alternatively, the application can be downloaded from the [package management](https://gitlab.com/drophoff/training-java/-/packages) of this project and then started in the same way.
The folder ends with the name application.standalone.
Within this folder there is a file named application.standalone-{VERSIONMAJOR}.{VERSIONMINOR}.{VERSIONPATCH}-{YYYYMMDD.HHMMSS}-VERSION.jar that represents the back-end application as Spring Boot Fat JAR.

The activation of profiles during the application start is an optional and not required to get the application up and running.


### Front-End

* Change into the directory: /application/client
* `npm install` to install the required npm packages
* `npm run serve` to start the front-end

### Docker-Support

There are also [docker images](https://gitlab.com/drophoff/training-java/container_registry) that can be used to start the application on a single host.
Currently only the amd64 architecture is supported.
The following commands are required to start the application as background docker container:
* `docker run -t -d --net=host --rm --name server registry.gitlab.com/drophoff/training-java/book-server-standalone:TAG`
* `docker run -t -d --net=host --rm --name client registry.gitlab.com/drophoff/training-java/book-client:TAG`

After the application has been started it can be accessed via `http://CONTAINER-HOST-IP:8080`.

## IDE-Support

The maven projects common-gen and server-gen contains generated artefacts.
Close these projects within your IDE to avoid not resolveable class file messages.

## API Consumption

### Via OpenAPI

Install OpenAPI CLI from [here](https://openapi-generator.tech/docs/installation).
A description for the Open CLI can be found [here](https://openapi-generator.tech/docs/usage).
The following command shows the API generator for a java client.
* `java -jar ./openapi-generator-cli.jar generate -g java -i ./book/openapi/rsrc/booking.yml -o ~/java-api-out`

The generated artifacts also contain descriptions for using the Java classes to call the Rest-Endpoints.

### Via Command Line

#### Save

```powershell
curl --location --request POST 'http://localhost:8081/rest/v1/book' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
    "name" : "Fullstack Vue: The Complete Guide",
    "price": 33.99,
    "isbn" : "9781987595291"
}'
```

#### Delete

```powershell
curl --location --request DELETE 'http://localhost:8081/rest/v1/book/9781987595291' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json'
```

#### List

```powershell
curl --location --request GET 'http://localhost:8081/rest/v1/book/listAll' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json'
```

## Runtime-View

The figure shows the classes and their methods that are executed when a record gets saved within the back-end.
```mermaid
%%{init: 
  { 
    'theme': 'forest',
    'sequence': { 
      'activationWidth': 8,
      'diagramMarginX': 1,
      'diagramMarginY': 10,
      'width': 100,
		  'actorMargin': 15,
		  'actorFontSize': 14,
		  'messageFontSize': 13
    }
  }
}%%
sequenceDiagram
    autonumber
    front-end->>+back-end: POST /rest/v1/book
    Note over back-end: Open Transaction
    back-end->>validator: validate BookDto
    back-end->>+maintenance: save BookDto
    maintenance->>+domain manager: save BookDto
    domain manager->>+mapper: map BookDto
    mapper-->>-domain manager: Book
    domain manager->>+dao: save Book
    dao-->>-domain manager: saved Book
    domain manager->>+mapper: map Book
    mapper-->>-domain manager: BookDto
    domain manager-->>-maintenance: saved BookDto
    maintenance-->>-back-end: saved BookDto
    Note over back-end: Close Transaction
    back-end-->>-front-end: 200 Ok, BookDto
```
