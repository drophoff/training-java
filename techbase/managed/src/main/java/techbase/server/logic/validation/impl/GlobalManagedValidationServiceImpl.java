package techbase.server.logic.validation.impl;

import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.GlobalValidator;
import techbase.server.logic.validation.MessageContainer;

@Named("GlobalValidationService")
@Singleton
public class GlobalManagedValidationServiceImpl extends AbstractGlobalValidationService {
    
    @Inject
    private Instance<GlobalValidator<?>> genericValidators;
    
    @SuppressWarnings("unchecked")
    public MessageContainer getValidationMessages(Object argument) {
        final MessageContainer messageContainer = new MessageContainer();
        
        for (GlobalValidator<?> validator : this.genericValidators) {
            final GlobalValidator<Object> validatorInstance = (GlobalValidator<Object>) validator;
            if (validatorInstance.isResponsibleFor().equals(argument.getClass())) {
                final List<ConstraintViolation> messages = validatorInstance.validate(argument);

                if (messages != null && !messages.isEmpty()) {
                    messageContainer.add(messages);
                }
            }
        }
        
        return messageContainer;
    }
}
