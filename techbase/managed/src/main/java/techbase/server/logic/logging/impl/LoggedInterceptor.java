package techbase.server.logic.logging.impl;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import techbase.server.logic.logging.Logged;

@Logged
@Dependent
@Interceptor
@Priority(Interceptor.Priority.APPLICATION)
public class LoggedInterceptor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@AroundInvoke
    Object aroundInvoke(InvocationContext context) throws Exception {
        return logMethodEntry(context);
    }

    @AroundTimeout
    Object aroundTimeout(InvocationContext context) throws Exception {
        return logMethodEntry(context);
    }
	
    private Object logMethodEntry(InvocationContext invocationContext) throws Exception {
		Object returnValue = null;

		final Method method = invocationContext.getMethod();
		final String className = method.getDeclaringClass().getName();
        final String methodName = method.getName();
        final Level level = LogLevelUtil.retrieve(method);
        final Logger logger = Logger.getLogger(className);
        
        log("Enter", className, methodName, logger, level);

        returnValue = invocationContext.proceed();

        log("Leave", className, methodName, logger, level);

        return returnValue;
	}
    
    private void log(final String prefix, final String className, final String methodName, final Logger logger, final Level level) {
    	final String message = prefix + ": {0}#{1}";
    	
    	logger.log(level, message, new Object[] {className, methodName });
    }
}
