package techbase.server.logic.validation.impl;

import java.util.List;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.LocalValidator;
import techbase.server.logic.validation.MessageContainer;

@Named("LocalValidationService")
@Singleton
public class LocalManagedValidationServiceImpl extends AbstractLocalValidationService {
    
    @Inject
    private Instance<LocalValidator<?>> instanceValidators;
    
    @SuppressWarnings("unchecked")
    public <Type> MessageContainer getValidationMessages(Type argument, Class<? extends LocalValidator<Type>> validator) {
        final MessageContainer messageContainer = new MessageContainer();
       
        for (LocalValidator<?> validatorInstanceWithoutType : this.instanceValidators) {
            final LocalValidator<Object> validatorInstance = (LocalValidator<Object>) validatorInstanceWithoutType;
            if (validatorInstance.getClass().equals(validator)) {
                final List<ConstraintViolation> messages = validatorInstance.validate(argument);

                if (messages != null && !messages.isEmpty()) {
                    messageContainer.add(messages);
                }
            }
        }
        
        return messageContainer;
    }
}
