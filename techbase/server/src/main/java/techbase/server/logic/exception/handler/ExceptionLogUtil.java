package techbase.server.logic.exception.handler;

import java.util.logging.Level;
import java.util.logging.Logger;

final class ExceptionLogUtil {
    private static final Logger LOG = Logger.getLogger("ExceptionHandler");

    private ExceptionLogUtil() {
    }

    static void logFailure(final String exceptionType, final Exception exception) {
    	LOG.log(Level.SEVERE, "Type: {0}, Class: {1}", new Object[] { exceptionType, exception });
    	LOG.log(Level.SEVERE, "Trace: ", exception);
    }
}
