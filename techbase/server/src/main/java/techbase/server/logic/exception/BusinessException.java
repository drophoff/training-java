package techbase.server.logic.exception;

/**
 * Represents a checked business exception.
 */
public abstract class BusinessException extends RuntimeException implements BaseException {
    private static final long serialVersionUID = -6407909229554453622L;
}
