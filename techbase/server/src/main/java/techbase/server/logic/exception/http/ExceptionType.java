package techbase.server.logic.exception.http;

public enum ExceptionType {
    TECHNICAL("Technical"),

    BUSINESS("Business"),

    RUNTIME("Runtime");

    private final String name;

    private ExceptionType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
