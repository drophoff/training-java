package techbase.server.logic.validation;

import java.util.List;

import techbase.server.logic.exception.http.ConstraintViolation;

public interface GlobalValidationService {

    /**
     * Performs all global available validations that are defined for the provided
     * argument.
     * 
     * @param argument that shall be validated
     * @throws ValidationException in case a validation fails
     */
    void validate(Object argument);

    /**
     * Performs all global available validations that are defined for the provided
     * argument.
     * 
     * @param argument argument that shall be validated
     * @return list of violation that are failing, never null.
     */
    List<ConstraintViolation> validateWithResult(Object argument);
}
