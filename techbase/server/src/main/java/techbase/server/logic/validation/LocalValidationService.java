package techbase.server.logic.validation;

import java.util.List;

import techbase.server.logic.exception.http.ConstraintViolation;

public interface LocalValidationService {

    /**
     * Performs only the validations that are defined by the provided validator.
     * 
     * @param <Type>    of the formal argument
     * @param argument  that shall be validated
     * @param validator that shall be used for the validation
     * @throws ValidationException in case a validation fails
     */
    <Type> void validate(Type argument, Class<? extends LocalValidator<Type>> validator);

    /**
     * Performs only the validations that are defined by the provided validator.
     * 
     * @param <Type>    of the formal argument
     * @param argument  that shall be validated
     * @param validator that shall be used for the validation
     * @return list of violation that are failing, never null.
     */
    <Type> List<ConstraintViolation> validateWithResult(Type argument, Class<? extends LocalValidator<Type>> validator);
}
