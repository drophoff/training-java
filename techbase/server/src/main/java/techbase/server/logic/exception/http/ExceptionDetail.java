package techbase.server.logic.exception.http;

/**
 * Contains detail information about an exception.
 */
public class ExceptionDetail {
    private final String code;
    private final String message;
    private final String detail;

    public ExceptionDetail() {
        this("000000", "An error has occurred.", "Please contact the IT support.");
    }

    public ExceptionDetail(final String code, final String message) {
        this(code, message, "N/A");
    }

    public ExceptionDetail(final String code, final String message, final String detail) {
        this.code = code;
        this.message = message;
        this.detail = detail;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getDetail() {
        return detail;
    }
}
