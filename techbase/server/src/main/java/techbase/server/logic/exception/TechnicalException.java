package techbase.server.logic.exception;

/**
 * Represents an unchecked technical exception.
 */
public abstract class TechnicalException extends RuntimeException implements BaseException {
    private static final long serialVersionUID = -7169093679001939733L;
}
