package techbase.server.logic.exception;

import techbase.server.logic.exception.http.ExceptionDetail;

public class ConcurrentModificationException extends BusinessException {
    private static final long serialVersionUID = 6245951348782165986L;
    private final String entity;
    private final transient Object id;

    public ConcurrentModificationException(final String entity, final Object id) {
        this.entity = entity;
        this.id = id;
    }

    @Override
    public ExceptionDetail getError() {
        return new ExceptionDetail("000003", "A concurrent modification has occurred!", "Entity: " + entity + " with ID: " + id.toString());
    }
}
