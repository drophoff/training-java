package techbase.server.logic.exception.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import techbase.server.logic.exception.BusinessException;
import techbase.server.logic.exception.http.ExceptionResponse;
import techbase.server.logic.exception.http.ExceptionType;

@Provider
public class BusinessExceptionHandler implements ExceptionMapper<BusinessException> {

    @Override
    public Response toResponse(BusinessException exception) {
        final ExceptionResponse response = new ExceptionResponse(ExceptionType.BUSINESS, exception.getError());

        ExceptionLogUtil.logFailure(response.getType(), exception);

        return ResponseUtil.build(Status.BAD_REQUEST, response);
    }

}
