package techbase.server.logic.validation.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.GlobalValidationService;
import techbase.server.logic.validation.LocalValidationService;
import techbase.server.logic.validation.LocalValidator;
import techbase.server.logic.validation.ValidatorService;

@Named
@Singleton
public class ValidatorServiceImpl implements ValidatorService {
    
	@Inject
	@Named("GlobalValidationService")
    private GlobalValidationService genericValidators;
	
	@Inject
	@Named("LocalValidationService")
    private LocalValidationService localValidators;

	@Override
    public void validate(Object argument) {
        this.genericValidators.validate(argument);
    }

	@Override
    public List<ConstraintViolation> validateWithResult(Object argument) {
        return this.genericValidators.validateWithResult(argument);
    }

	@Override
    public <Type> void validate(Type argument, Class<? extends LocalValidator<Type>> validator) {
        this.localValidators.validate(argument, validator);
    }

	@Override
    public <Type> List<ConstraintViolation> validateWithResult(Type argument, Class<? extends LocalValidator<Type>> validator) {
        return this.localValidators.validateWithResult(argument, validator);
    }
}
