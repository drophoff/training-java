package techbase.server.logic.validation.impl;

import java.util.ArrayList;
import java.util.List;

import techbase.server.logic.exception.ValidationException;
import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.GlobalValidationService;
import techbase.server.logic.validation.MessageContainer;

public abstract class AbstractGlobalValidationService implements GlobalValidationService {
    
    @Override
    public void validate(Object argument) {
        final List<ConstraintViolation> constraintViolations = validateWithResult(argument);
        
        if (!constraintViolations.isEmpty()) {
            throw new ValidationException(constraintViolations);
        }
    }

    @Override
    public List<ConstraintViolation> validateWithResult(Object argument) {
        final List<ConstraintViolation> constraintViolations = new ArrayList<>();
        
        if (argument != null) {
            final MessageContainer messageContainer = getValidationMessages(argument);
            
            if (!messageContainer.isEmpty()) {
                constraintViolations.addAll(messageContainer.get());
            }
        }
        
        return constraintViolations;
    }
    
    /**
     * Performs all validations that are defined for the provided argument (see
     * {@link Validator} interface for further information).
     * 
     * @param argument that shall be validated
     * @return a container with all violation messages, never null.
     */
    abstract MessageContainer getValidationMessages(Object argument);
}
