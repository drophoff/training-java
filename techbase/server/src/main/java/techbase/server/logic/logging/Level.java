package techbase.server.logic.logging;

public enum Level {
	ERROR("SEVERE"),
	WARN("WARNING"),
	INFO("INFO"),
	DEBUG("FINE"),
	TRACE("FINEST");
	
	private String name;

	/**
	 * Used to map the reusable enumeration constant value to the corresponding
	 * Java Logging API level names.
	 * 
	 * @param logLevelName that is used internally. 
	 */
	private Level(String logLevelName) {
		this.name = logLevelName;
	}

	public String getName() {
		return name;
	}
}
