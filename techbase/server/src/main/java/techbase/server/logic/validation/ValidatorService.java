package techbase.server.logic.validation;

/**
 * Generic validation service that performs validation checks defined by the
 * {@link Validator} interface.
 */
public interface ValidatorService extends GlobalValidationService, LocalValidationService {
}
