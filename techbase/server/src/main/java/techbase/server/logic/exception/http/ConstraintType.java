package techbase.server.logic.exception.http;

/**
 * Defines the type of a constraint.
 */
public enum ConstraintType {
    /**
     * Pattern mismatch or content not valid
     */
    PATTERN,

    /**
     * Underflow or too short
     */
    MIN,

    /**
     * Overflow or too long
     */
    MAX,

    /**
     * Missing
     */
    REQUIRED,

    /**
     * Key is not unique
     */
    UNIQUE;
}
