package techbase.server.logic.exception.handler;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import techbase.server.logic.exception.http.ExceptionDetail;
import techbase.server.logic.exception.http.ExceptionResponse;
import techbase.server.logic.exception.http.ExceptionType;

/**
 * Handles all JAX-RS failures like resource not found or bad request. This
 * class has a higher priority than WebApplicationExceptionMapper.
 */
@Provider
public class GenericExceptionHandler implements ExceptionMapper<WebApplicationException> {

    @Override
    public Response toResponse(WebApplicationException exception) {
        final ExceptionResponse response = new ExceptionResponse(ExceptionType.RUNTIME, new ExceptionDetail());

        ExceptionLogUtil.logFailure(response.getType(), exception);

        return ResponseUtil.build(Status.INTERNAL_SERVER_ERROR, response);
    }
}
