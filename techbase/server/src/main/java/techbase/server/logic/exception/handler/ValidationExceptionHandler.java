package techbase.server.logic.exception.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import techbase.server.logic.exception.ValidationException;
import techbase.server.logic.exception.http.ExceptionType;
import techbase.server.logic.exception.http.ValidationResponse;

@Provider
public class ValidationExceptionHandler implements ExceptionMapper<ValidationException> {

    @Override
    public Response toResponse(ValidationException exception) {
        final ValidationResponse response = new ValidationResponse(ExceptionType.BUSINESS, exception.getError(), exception.getViolations());

        ExceptionLogUtil.logFailure(response.getType(), exception);

        return ResponseUtil.build(Status.BAD_REQUEST, response);
    }

}
