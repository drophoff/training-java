package techbase.server.logic.validation;

import java.util.List;

import techbase.server.logic.exception.http.ConstraintViolation;

/**
 * Each concrete validator must implement this interface.
 * 
 * @param <Type> describes the object that shall be validated
 */
public interface Validator<Type> {

    /**
     * Contains the validation logic that will be executed for the desired
     * object provided by the first formal argument.
     * 
     * @param type that should be validated.
     * @return list of possible validation failures. Null is allowed and valid
     * return value.
     */
    List<ConstraintViolation> validate(final Type type);
}
