package techbase.server.logic.exception.handler;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import techbase.server.logic.exception.http.ExceptionResponse;

final class ResponseUtil {
    private ResponseUtil() {
    }

    static Response build(final Status status, final ExceptionResponse exception) {
        return Response.status(status).entity(exception).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON).build();
    }
}
