package techbase.server.logic.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;


/**
 * Logs the method invocation with the method name itself and the surrounding
 * class name.
 */
@Inherited
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Logged {
	
    /**
     * The level of detail to log at. If none is specified {@link Level#DEBUG}
     * is used as default.
     *
     * @return the log level that should be used.
     */
    @Nonbinding
    public Level value() default Level.DEBUG;
}
