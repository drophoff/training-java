package techbase.server.logic.validation;

/**
 * Validates an object by a local reference.
 * 
 * @param <Type> describes the object that shall be validated
 */
public interface LocalValidator<Type> extends Validator<Type> {
}
