package techbase.server.logic.exception.http;

public class ConstraintViolation {
    private final ConstraintType type;
    private final String message;

    public ConstraintViolation(final ConstraintType type, final String message) {
        this.type = type;
        this.message = message;
    }

    public ConstraintType getType() {
        return type;
    }

    public String getMessage() {
        return this.message;
    }
}
