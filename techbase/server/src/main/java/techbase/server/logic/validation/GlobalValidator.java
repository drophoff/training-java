package techbase.server.logic.validation;

/**
 * Validates objects for which he is responsible for. There may be multiple
 * validators, which are working on identical objects.
 * 
 * @param <Type> describes the object that shall be validated
 */
public interface GlobalValidator<Type> extends Validator<Type> {

    /**
     * Defines the class for which the validator is responsible for and performing
     * validation.
     * 
     * @return the class that shall be validated, should be never null.
     */
    Class<Type> isResponsibleFor();
}
