package techbase.server.logic.exception.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import techbase.server.logic.exception.TechnicalException;
import techbase.server.logic.exception.http.ExceptionResponse;
import techbase.server.logic.exception.http.ExceptionType;

@Provider
public class TechnicalExceptionHandler implements ExceptionMapper<TechnicalException> {

    @Override
    public Response toResponse(TechnicalException exception) {
        final ExceptionResponse response = new ExceptionResponse(ExceptionType.TECHNICAL, exception.getError());

        ExceptionLogUtil.logFailure(response.getType(), exception);

        return ResponseUtil.build(Status.INTERNAL_SERVER_ERROR, response);
    }

}
