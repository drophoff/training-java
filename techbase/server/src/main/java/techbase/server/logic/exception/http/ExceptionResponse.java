package techbase.server.logic.exception.http;

import java.time.ZonedDateTime;

/**
 * Wrapper class for the exception details that will be transmitted via http.
 */
public class ExceptionResponse extends ExceptionDetail {
    private final ExceptionType type;
    private final ZonedDateTime time;

    public ExceptionResponse(final ExceptionType type, final ExceptionDetail detail) {
        super(detail.getCode(), detail.getMessage(), detail.getDetail());
        this.time = ZonedDateTime.now();
        this.type = type;
    }

    public String getType() {
        return this.type.toString();
    }

    public String getTime() {
        return this.time.toString();
    }
}
