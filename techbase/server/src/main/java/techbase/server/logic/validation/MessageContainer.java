package techbase.server.logic.validation;

import java.util.ArrayList;
import java.util.List;

import techbase.server.logic.exception.http.ConstraintViolation;

/**
 * Container for validation messages.
 */
public class MessageContainer {
    List<ConstraintViolation> messages = new ArrayList<>();

    /**
     * Adds the provides list of validation messages to the existing list of
     * validation messages.
     * 
     * @param messages that should be added.
     */
    public void add(final List<ConstraintViolation> messages) {
        this.messages.addAll(messages);
    }

    /**
     * @return a list of validation messages.
     */
    public List<ConstraintViolation> get() {
        return this.messages;
    }

    /**
     * Returns true in case there is no validation messages, otherwise false.
     * 
     * @return true or false.
     */
    public boolean isEmpty() {
        return this.messages.isEmpty();
    }
}
