package techbase.server.logic.logging.impl;

import java.lang.reflect.Method;
import java.util.logging.Level;

import techbase.server.logic.logging.Logged;

public class LogLevelUtil {
	private LogLevelUtil() {
		// Avoid instance creation
	}
	
    public static Level retrieve(final Method method) {
    	Level level = Level.FINE;
    	
    	final Logged classLevel = method.getDeclaringClass().getAnnotation(Logged.class);
    	if (classLevel != null) {
    		level = parseLevel(classLevel.value(), level);
    	}

    	final Logged methodLevel = method.getAnnotation(Logged.class);
    	if (methodLevel != null) {
    		level = parseLevel(methodLevel.value(), level);
    	}
    	
    	return level;
    }
    
    private static Level parseLevel(techbase.server.logic.logging.Level provided, Level fallback) {
    	Level parsed = fallback;
    	
    	try {
    		parsed = Level.parse(provided.getName());
    	} catch (IllegalArgumentException iae) {
    		/*
    		 * In case the level is not known or can not be parsed.
    		 * No exception handling needed due to fallback logic. 
    		 */
    	}
    	
    	return parsed;
    }
}
