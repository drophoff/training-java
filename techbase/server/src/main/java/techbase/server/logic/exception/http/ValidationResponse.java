package techbase.server.logic.exception.http;

import java.util.List;

public class ValidationResponse extends ExceptionResponse {
    private final List<ConstraintViolation> constraints;

    public ValidationResponse(ExceptionType type, final ExceptionDetail detail, List<ConstraintViolation> constraintViolations) {
        super(type, detail);
        this.constraints = constraintViolations;
    }

    public List<ConstraintViolation> getConstraints() {
        return constraints;
    }
}
