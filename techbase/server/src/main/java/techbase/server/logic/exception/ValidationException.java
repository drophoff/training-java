package techbase.server.logic.exception;

import java.util.List;

import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.exception.http.ExceptionDetail;

/**
 * Indicates that a validation check failed while performing a validation
 * operation.
 */
public class ValidationException extends BusinessException {
    private static final long serialVersionUID = -4310672728296720033L;
    private final transient List<ConstraintViolation> constraintViolations;

    public ValidationException(List<ConstraintViolation> constraintViolations) {
        this.constraintViolations = constraintViolations;
    }

    @Override
    public ExceptionDetail getError() {
        return new ExceptionDetail("000002", "Validation failed", "Constraints checks are violated!");
    }

    public List<ConstraintViolation> getViolations() {
        return constraintViolations;
    }
}
