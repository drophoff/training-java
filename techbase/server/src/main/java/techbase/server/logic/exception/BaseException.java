package techbase.server.logic.exception;

import techbase.server.logic.exception.http.ExceptionDetail;

/**
 * Defines a minimal interface that must be implemented by every excetpion.
 */
public interface BaseException {

    /**
     * @return Detail information about the occurred exception
     */
    ExceptionDetail getError();
}
