package techbase.server.logic.validation.impl;

import java.util.ArrayList;
import java.util.List;

import techbase.server.logic.exception.ValidationException;
import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.LocalValidationService;
import techbase.server.logic.validation.LocalValidator;
import techbase.server.logic.validation.MessageContainer;

public abstract class AbstractLocalValidationService implements LocalValidationService {
    
    @Override
    public <Type> void validate(Type argument, Class<? extends LocalValidator<Type>> validator) {
        final List<ConstraintViolation> constraintViolations = validateWithResult(argument, validator);
        
        if (!constraintViolations.isEmpty()) {
            throw new ValidationException(constraintViolations);
        }
    }
    
    @Override
    public <Type> List<ConstraintViolation> validateWithResult(Type argument, Class<? extends LocalValidator<Type>> validator) {
        final List<ConstraintViolation> constraintViolations = new ArrayList<>();
        
        if (argument != null && validator != null) {
            final MessageContainer messageContainer = getValidationMessages(argument, validator);
            
            if (!messageContainer.isEmpty()) {
                constraintViolations.addAll(messageContainer.get());
            }
        }
        
        return constraintViolations;
    }
    
    /**
     * Performs only the validations that are defined by the provided validator.
     * 
     * @param <Type>
     * @param argument that shall be validated.
     * @param validator a container with the violation messages.
     * @return a container with all violation messages, never null.
     */
    abstract <Type> MessageContainer getValidationMessages(Type argument, Class<? extends LocalValidator<Type>> validator);
}
