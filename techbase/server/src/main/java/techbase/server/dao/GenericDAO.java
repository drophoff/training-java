package techbase.server.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class GenericDAO<T> {
	
	@PersistenceContext
	private EntityManager em;

	public T save(T t) {
		T result = null;
		
		if (t != null) {
			result = getEntityManager().merge(t);
			
			getEntityManager().flush();
		}
		
		return result;
	}
	
	public EntityManager getEntityManager() {
		return this.em;
	}
}
