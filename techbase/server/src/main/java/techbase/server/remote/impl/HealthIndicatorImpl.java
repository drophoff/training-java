package techbase.server.remote.impl;

import java.util.Collections;
import java.util.Map;

import javax.ws.rs.core.Response;

import techbase.server.remote.HealthIndicator;

public class HealthIndicatorImpl implements HealthIndicator {

    @Override
    public Response live() {
    	final Map<String, String> ok = Collections.singletonMap("status", "ok");
    	return Response.ok(ok).build();
    }
}
