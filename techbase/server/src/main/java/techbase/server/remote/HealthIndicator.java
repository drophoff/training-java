package techbase.server.remote;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This class contains a simple rest endpoint to monitor the health of the
 * provided application.
 */
@Path("/v1/health")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface HealthIndicator {
	
    /**
     * Trivial health check that returns always OK, when the application is live and responsive.
     * 
     * @return always OK
     */
    @GET
    @Path("/live")
    Response live();
}
