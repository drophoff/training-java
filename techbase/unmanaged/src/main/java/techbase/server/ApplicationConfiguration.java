package techbase.server;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import techbase.server.logic.config.values.GlobalConfiguration;
import techbase.server.logic.logging.impl.LoggingConfiguration;
import techbase.server.logic.validation.impl.ValidationConfiguration;

@Import({ GlobalConfiguration.class, LoggingConfiguration.class, ValidationConfiguration.class })
@Configuration
public class ApplicationConfiguration {
}
