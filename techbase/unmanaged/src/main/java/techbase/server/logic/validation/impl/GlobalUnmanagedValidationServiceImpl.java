package techbase.server.logic.validation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.springframework.beans.factory.annotation.Autowired;

import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.GlobalValidator;
import techbase.server.logic.validation.MessageContainer;

@Named("GlobalValidationService")
@Singleton
public class GlobalUnmanagedValidationServiceImpl extends AbstractGlobalValidationService {
    private List<GlobalValidator<Object>> globalValidators = new ArrayList<>();
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Autowired(required = false)
    public void setGenericValidators(List<GlobalValidator> globalValidators) {
        if (globalValidators != null) {
            for (int index = 0; index < globalValidators.size(); index++) {
                GlobalValidator<Object> validator = (GlobalValidator<Object>) globalValidators.get(index);
                this.globalValidators.add(validator);
            }
        }
    }
    
    @Override
    public MessageContainer getValidationMessages(Object argument) {
        final MessageContainer messageContainer = new MessageContainer();

        for (GlobalValidator<Object> validator : this.globalValidators) {
            if (argument.getClass().equals(validator.isResponsibleFor())) {
                final List<ConstraintViolation> messages = validator.validate(argument);
                
                if (messages != null && !messages.isEmpty()) {
                    messageContainer.add(messages);
                }
            }
        }

        return messageContainer;
    }
}
