package techbase.server.logic.logging.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class LoggingAspect {

	@Around("execution(* (@techbase.server.logic.logging.Logged *).*(..))")
	public Object logAnnotatedClassesOrMethods(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		return logMethodExecution(proceedingJoinPoint);
	}

    private Object logMethodExecution(final ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        Object returnValue = null;

        final MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        final String className = methodSignature.getDeclaringType().getName();
        final String methodName = methodSignature.getName();
        final Level level = LogLevelUtil.retrieve(methodSignature.getMethod());
        final Logger logger = Logger.getLogger(className);

        logger.log(level, "Enter: {0}", new Object[] { methodName });

        returnValue = proceedingJoinPoint.proceed();

        logger.log(level, "Leave: {0}", new Object[] { methodName });

        return returnValue;
    }
}
