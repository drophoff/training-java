package techbase.server.logic.validation.impl;

import javax.inject.Named;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import techbase.server.logic.validation.GlobalValidationService;
import techbase.server.logic.validation.LocalValidationService;
import techbase.server.logic.validation.ValidatorService;

@Configuration
public class ValidationConfiguration {
    
    @Bean
    @Named("GlobalValidationService")
    public GlobalValidationService createGlobalValidationService() {
        return new GlobalUnmanagedValidationServiceImpl();
    }
    
    @Bean
    @Named("LocalValidationService")
    public LocalValidationService createLocalValidationService() {
        return new LocalUnmanagedValidationServiceImpl();
    }

    @Bean
    public ValidatorService createValidatorService() {
        return new ValidatorServiceImpl();
    }
}
