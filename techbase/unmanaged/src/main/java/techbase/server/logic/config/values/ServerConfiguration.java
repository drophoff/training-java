package techbase.server.logic.config.values;

public class ServerConfiguration {
    private Integer port;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
