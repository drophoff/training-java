package techbase.server.logic.exception.handler;

import javax.persistence.OptimisticLockException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.hibernate.StaleObjectStateException;

import techbase.server.logic.exception.ConcurrentModificationException;
import techbase.server.logic.exception.http.ExceptionResponse;
import techbase.server.logic.exception.http.ExceptionType;

@Provider
public class OptimisticLockingExceptionHandler implements ExceptionMapper<OptimisticLockException> {

    @Override
    public Response toResponse(OptimisticLockException exception) {
    	final StaleObjectStateException rootException = (StaleObjectStateException) exception.getCause();
    	
        final ExceptionResponse response = new ExceptionResponse(ExceptionType.RUNTIME, new ConcurrentModificationException(rootException.getEntityName(), rootException.getIdentifier()).getError());

        ExceptionLogUtil.logFailure(response.getType(), exception);

        return ResponseUtil.build(Status.INTERNAL_SERVER_ERROR, response);
    }

}
