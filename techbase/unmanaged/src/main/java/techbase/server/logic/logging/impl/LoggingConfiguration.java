package techbase.server.logic.logging.impl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("logging")	
public class LoggingConfiguration {

    @Bean
    public LoggingAspect createGlobalLoggingAspect() {
        return new LoggingAspect();
    }
}
