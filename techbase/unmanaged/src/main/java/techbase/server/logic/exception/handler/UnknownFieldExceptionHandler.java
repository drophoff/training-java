package techbase.server.logic.exception.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.exc.UnrecognizedPropertyException;

import techbase.server.logic.exception.http.ExceptionDetail;
import techbase.server.logic.exception.http.ExceptionResponse;
import techbase.server.logic.exception.http.ExceptionType;

@Provider
public class UnknownFieldExceptionHandler implements ExceptionMapper<UnrecognizedPropertyException> {

    @Override
    public Response toResponse(UnrecognizedPropertyException exception) {
        final ExceptionResponse response = new ExceptionResponse(ExceptionType.RUNTIME, new ExceptionDetail("000004", "Mapping Failure", "Unrecognized field: " + exception.getUnrecognizedPropertyName()));

        ExceptionLogUtil.logFailure(response.getType(), exception);

        return ResponseUtil.build(Status.BAD_REQUEST, response);
    }

}
