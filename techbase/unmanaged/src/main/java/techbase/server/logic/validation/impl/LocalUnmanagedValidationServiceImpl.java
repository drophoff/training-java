package techbase.server.logic.validation.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.beans.factory.annotation.Autowired;

import techbase.server.logic.exception.http.ConstraintViolation;
import techbase.server.logic.validation.LocalValidator;
import techbase.server.logic.validation.MessageContainer;

@Named("LocalValidationService")
@Singleton
public class LocalUnmanagedValidationServiceImpl extends AbstractLocalValidationService {
    private List<LocalValidator<Object>> localValidators = new ArrayList<>();
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Autowired(required = false)
    public void setLocalValidators(List<LocalValidator> localValidators) {
        if (localValidators != null) {
            for (int index = 0; index < localValidators.size(); index++) {
                final LocalValidator<Object> validator = (LocalValidator<Object>) localValidators.get(index);
                this.localValidators.add(validator);
            }
        }
    }

    @Override
    public <Type> MessageContainer getValidationMessages(Type argument, Class<? extends LocalValidator<Type>> validator) {
        final MessageContainer messageContainer = new MessageContainer();
        
        for (LocalValidator<Object> validatorInstance : this.localValidators) {
            if (validator.equals(AopProxyUtils.ultimateTargetClass(validatorInstance))) {
            	final List<ConstraintViolation> messages = validatorInstance.validate(argument);
                
                if (messages != null && !messages.isEmpty()) {
                    messageContainer.add(messages);
                }
            }
        }
        
        return messageContainer;
    }
}
